import {Navigate} from "react-router-dom";
import {connect} from "react-redux";

function LoginOrRegistration(props) {
    const loggedIn = props.auth != null;

    return !loggedIn ? props.element : <Navigate to="/"/>
}

function mapStateToProps(state) {
    return {
        auth: state.auth
    }
}

export default connect(mapStateToProps)(LoginOrRegistration)
