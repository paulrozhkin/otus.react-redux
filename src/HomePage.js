import React from 'react';
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {Button} from "react-bootstrap";
import {logout} from "./actions/logout";

function HomePage(props) {
    return (
        <div>
            <h1>Home Page</h1>
            <h2>Hello, {props.user.login}</h2>
            <Button onClick={() => props.logout()}>Logout</Button>
        </div>
    )
}

function mapStateToProps(state) {
    return {
        user: state.auth
    }
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({logout: logout}, dispatch)
}

export default connect(mapStateToProps, matchDispatchToProps)(HomePage);
