import store from "../store";

export default function (state = null, action) {
    switch (action.type) {
        case "LOGIN":
            const users = action.payload.users;

            const credentials = action.payload;
            const loggedUser = users.find(user => user.login === credentials.login)
            if (loggedUser !== undefined && loggedUser.password === credentials.password) {
                return loggedUser;
            }
            else {
                throw "Invalid username or password"
            }
            break;
        case "LOGOUT":
            return null;
        default:
            return state;
    }
}
