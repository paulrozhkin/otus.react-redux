import {combineReducers} from "redux";
import UsersReducers from './users'
import AuthUser from "./auth-user";

const allReducers = combineReducers({
   users: UsersReducers,
   auth: AuthUser
});

export default allReducers
