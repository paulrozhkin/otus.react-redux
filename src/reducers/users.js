const defaultUsers = [
    {
        login: "admin@gmail.com",
        password: "123456"
    }
];

export default function (state = defaultUsers, action) {
    switch (action.type) {
        case "REGISTER":
            const userToFind = action.payload.users.find(x => x.login === action.payload.login)
            if (userToFind !== undefined) {
                throw 'User already registered'
            }

            return [defaultUsers, {login: action.payload.login, password: action.payload.password}]
        default:
            return state;
    }
}
