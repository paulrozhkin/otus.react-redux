import './App.css';
import {
    BrowserRouter as Router, Route, Routes
} from "react-router-dom";
import {Provider} from 'react-redux'
import store from "./store";
import {Col, Container, Row} from "react-bootstrap";
import PrivateRoute from "./PrivateRoute";
import HomePage from "./HomePage";
import LoginOrRegistration from "./LoginOrRegistration";
import Login from "./Login";
import PageNotFound from "./PageNotFound";

function App() {
    return (
        <Provider store={store}>
            <Router>
                <div className="App">
                    <Container className="h-100" fluid>
                        <Row className="justify-content-center align-items-center h-100">
                            <Col xl={3} lg={4} md={6} sm={6} className="login-form">
                                <Routes>
                                    <Route exact path="/" element={<PrivateRoute element={<HomePage/>}/>}/>
                                    <Route exact path="/login" element={<LoginOrRegistration element={<Login/>}/>}/>
                                    <Route exact path="/register"
                                           element={<LoginOrRegistration element={<Register/>}/>}/>
                                    <Route path="*" element={<PageNotFound/>}/>
                                </Routes>
                            </Col>
                        </Row>
                    </Container>
                </div>
            </Router>
        </Provider>
    );
}

function Register() {
    return <Login isRegister={true}/>
}

export default App;
