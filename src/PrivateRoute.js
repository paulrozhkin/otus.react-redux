import {Navigate} from "react-router-dom";
import {connect} from "react-redux";

function PrivateRoute(props) {
    const loggedIn = props.auth != null;

    return loggedIn ? props.element : <Navigate to="/login"/>
}

function mapStateToProps(state) {
    return {
        auth: state.auth
    }
}

export default connect(mapStateToProps)(PrivateRoute)
