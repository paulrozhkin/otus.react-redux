import {Form} from "react-bootstrap";
import {useState} from "react";
import {login} from './actions/login'
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {register} from "./actions/register";
import {Link, useNavigate} from "react-router-dom";
import store from "./store";

function Login(props) {
    const [form, setForm] = useState({})
    const [errors, setErrors] = useState({})
    const navigate = useNavigate();

    const isRegister = props.isRegister

    const setField = (field, value) => {
        setForm({
            ...form,
            [field]: value
        })

        // Check and see if errors exist, and remove them from the error object:
        if ( !!errors[field] ) setErrors({
            ...errors,
            [field]: null
        })
    }

    const findFormErrors = () => {
        const {email, password} = form
        const newErrors = {}
        // email errors
        if (!email || email === '') newErrors.email = 'cannot be blank!'
        else if (email.length > 50) newErrors.email = 'email is too long!'
        // password errors
        if (!password || password === '') newErrors.password = 'cannot be blank!'
        else if (password.length < 6) newErrors.password = 'password is too short!'

        return newErrors
    }

    function handleSubmit(event) {
        event.preventDefault();

        // get our new errors
        const newErrors = findFormErrors()

        // Conditional logic:
        if (Object.keys(newErrors).length > 0) {
            // We got errors!
            setErrors(newErrors)
        } else {
            if (isRegister) {
                try {
                    props.register({
                        login: form['email'],
                        password: form['password'],
                        users: store.getState().users
                    })
                    navigate('/login');
                }
                catch (error) {
                    console.log(error)
                    alert('User already registered')
                }
            } else {
                try {
                    props.login({
                        login: form['email'],
                        password: form['password'],
                        users: store.getState().users
                    })
                }
                catch (error) {
                    console.log(error)
                    alert('Invalid login or password')
                }

            }
        }
    }

    const loginText = isRegister ? "Sign Up" : "Sign In"

    return (
        <Form onSubmit={handleSubmit}>
            <h3>{loginText}</h3>

            <Form.Group>
                <Form.Label>Email address</Form.Label>
                <Form.Control type="email" placeholder="Enter email" onChange={e => setField('email', e.target.value)}
                              isInvalid={!!errors.email}/>
                <Form.Control.Feedback type='invalid'>
                    {errors.email}
                </Form.Control.Feedback>
            </Form.Group>

            <Form.Group>
                <label>Password</label>
                <Form.Control type="password" placeholder="Enter password"
                              onChange={e => setField('password', e.target.value)}
                              isInvalid={!!errors.password}/>
                <Form.Control.Feedback type='invalid'>
                    {errors.password}
                </Form.Control.Feedback>
            </Form.Group>

            <div className="btn-group">
                <button type="submit" className="btn btn-primary btn-block">Submit</button>
            </div>

            {!isRegister && (<p className="forgot-password text-right">
                Forgot <a href="#">password?</a>
            </p>)}

            {!isRegister && (<p className="forgot-password text-right">
                Does not have <Link to="/register">account?</Link>
            </p>)}

            {isRegister && (<p className="forgot-password text-right">
                Has <Link to="/login">account?</Link>
            </p>)}
        </Form>
    );
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({login: login, register: register}, dispatch)
}

export default connect(null, matchDispatchToProps)(Login);
