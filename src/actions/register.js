export const register = (credentials) => {
    return {
        type: "REGISTER",
        payload: credentials
    }
}
